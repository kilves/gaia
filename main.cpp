

#include "FastNoiseSIMD.h"
#include <iostream>
#include <ctime>
#include <cmath>
#include <cstring>
#include <string>

#include "Magick++.h"
#include "biomes.h"
#include "helpers.hpp"

using namespace std;

class FastNoiseSIMD;

void GetPixel(const GeneratorParameters& parameters, MagickLib::FloatPixelPacket& pixel, float elevation, float temperature, float rainfall, float latitude, float inlandness = 0.0f) {
    float avgTemperature = 0.0f;
    float avgRainFall = 0.5f;
    float distanceFromEquator = abs(latitude - 0.5f) * 2.0f;

    if(elevation < OCEAN_LEVEL) {
        float ocean_value = elevation / OCEAN_LEVEL;
        if(ocean_value < 0.5f)
            ocean_value = 0.5f;
        pixel = lerp(BLACK, WATER, elevation / OCEAN_LEVEL);
        return;
    }

    avgTemperature = 1.0f - sin(distanceFromEquator * PI/2);
    if(distanceFromEquator < 0.15f)
        avgRainFall = lerp(1.0f, 0.5f, pow(sin(distanceFromEquator / 0.15f * PI/2), parameters.rainfallSteepness));
    else if(distanceFromEquator < 0.45f)
        avgRainFall = lerp(0.0f, 0.5f, pow(sin(abs(distanceFromEquator - 0.3f) / 0.15f * PI/2), parameters.rainfallSteepness));
    else if(distanceFromEquator < 0.75f)
        avgRainFall = lerp(1.0f, 0.5f, pow(sin(abs(distanceFromEquator - 0.6f) / 0.15f * PI/2), parameters.rainfallSteepness));
    else
        avgRainFall = lerp(0.0f, 0.5f, (1.0f - distanceFromEquator) / 0.25f);
    float temperatureInlandFactor = pow(1.0f + (inlandness - parameters.oceanLevel) / (1.0f - parameters.oceanLevel) - 0.5f, parameters.temperatureExtremeness);
    float rainfallInlandFactor = pow(1.0f + (inlandness - parameters.oceanLevel) / (1.0f - parameters.oceanLevel) - 0.5f, parameters.temperatureExtremeness);
    avgTemperature = (avgTemperature - 0.5f)*temperatureInlandFactor + 0.5f*temperatureInlandFactor;
    avgRainFall = (avgRainFall - 0.5f)*rainfallInlandFactor + 0.5f*rainfallInlandFactor;
    if(avgTemperature > 1.0f)
        avgTemperature = 1.0f;
    if(avgRainFall > 1.0f)
        avgRainFall = 1.0f;

    avgTemperature = lerp(avgTemperature, 0.0f, (elevation - parameters.oceanLevel) * (1.0f - distanceFromEquator));
    avgTemperature = lerp(avgTemperature, 1.0f, (1.0f - (elevation - parameters.oceanLevel)) * (1.0f - distanceFromEquator));
    temperature = avgTemperature + (temperature - 0.5f) * (0.5f - abs(avgTemperature - 0.5f));
    temperature = lerp(temperature, 0.0f, (elevation - parameters.oceanLevel) / (1.0f - parameters.oceanLevel) * parameters.elevationColdness);
    rainfall = avgRainFall + (rainfall - 0.5f) * (0.5f - abs(avgRainFall - 0.5f));

    if(temperature > 1.0f)
        temperature = 1.0f;
    if(rainfall > 1.0f)
        rainfall = 1.0f;
    if(temperature < 0.0f)
        temperature = 0.0f;
    if(rainfall < 0.0f)
        rainfall = 0.0f;

    int left = (int)floor(rainfall*9);
    int right = (int)ceil(rainfall*9);
    int top = (int)floor(temperature*9);
    int bottom = (int)ceil(temperature*9);
    auto topLeft = BIOME[10 * top + left];
    auto topRight = BIOME[10 * top + right];
    auto bottomLeft = BIOME[10 * bottom + left];
    auto bottomRight = BIOME[10 * bottom + right];
    pixel = bicubify(topLeft, topRight, bottomLeft, bottomRight, rainfall * 9 - floor(rainfall * 9), temperature * 9 - floor(temperature * 9));
    //pixel = lerp(pixel, WHITE_PIXEL, elevation * 0.5f);
}

int main(int argc, char* argv[])
{
    GeneratorParameters params = {
        0.6f, 4.0f, 2.0f, 2.0f, 1.0f,
        400, 1337,
        "terrain.png"
    };
    parseParameters(params, argc, argv);
    int seed = params.seed;
    string outfile = params.outfile;
    int h = params.height;
    int w = h * 2;
    float fw = (float)w;
    float fh = (float)h;

    Magick::InitializeMagick("");

    FastNoiseSIMD* myNoise = FastNoiseSIMD::NewFastNoiseSIMD();
    myNoise->SetSeed(seed);
    myNoise->SetFrequency(0.002);
    myNoise->SetFractalOctaves(8);
    Magick::Image image;
    Magick::Image heightMap;
    MagickLib::FloatPixelPacket* pixels = new MagickLib::FloatPixelPacket[w * h];
    MagickLib::FloatPixelPacket* heightPixels = new MagickLib::FloatPixelPacket[w * h];
    int index = 0;
    float cx, cy, cz;
    int ix, iy, iz;
    float* elevation;
    float* temperature;
    float* rainfall;
    float* inlandness;
    cout << "Generating your world..." << endl
         << " 0%|                    |100%" << endl << "    ";
    for(int x = 0; x < w; x++) {
        if(x % (w / 20) == (w / 20) - 1) {
            cout << "=";
            flush(cout);
        }
        for(int y = 0; y < h; y++) {
            int idx = w * y + x;
            float fx = (float)x;
            float fy = (float)y;
            polarToCartesian(fx/fw*PI2, fy/fh*PI, 400.0f, cx, cy, cz);
            ix = (int)cx; iy = (int)cy; iz = (int)cz;
            elevation = myNoise->GetSimplexFractalSet(ix, iy, iz, 1, 1, 1);
            GetHeightPixel(heightPixels[idx], *elevation);
            temperature = myNoise->GetSimplexFractalSet(ix, iy, iz + 10000, 1, 1, 1);
            rainfall = myNoise->GetSimplexFractalSet(ix, iy, iz - 10000, 1, 1, 1);
            myNoise->SetFractalOctaves(4);
            inlandness = myNoise->GetSimplexFractalSet(ix, iy, iz, 1, 1, 1);
            myNoise->SetFractalOctaves(8);
            float e = (elevation[0] + 1.0f) / 2.0f;
            float t = (temperature[0] + 1.0f) / 2.0f;
            float r = (rainfall[0] + 1.0f) / 2.0f;
            float i = (inlandness[0] + 1.0f) / 2.0f;
            const float steepness = 0.8f;
            GetPixel(params, pixels[idx], pow(e, steepness), t, r, (float)y / (float)h, pow(i, steepness));
            index++;
        }
    }
    image.read(w, h, "RGBA", Magick::FloatPixel, pixels);
    heightMap.read(w, h, "RGBA", Magick::FloatPixel, heightPixels);
    image.write(outfile);
    heightMap.write("height." + outfile);
    cout << endl << "Written to "<< outfile << ", height map to height." << outfile << endl;

    FastNoiseSIMD::FreeNoiseSet(elevation);
    FastNoiseSIMD::FreeNoiseSet(temperature);
    FastNoiseSIMD::FreeNoiseSet(rainfall);
    FastNoiseSIMD::FreeNoiseSet(inlandness);
    delete myNoise;
    delete pixels;
    delete heightPixels;
    return 0;
}
