#------------------------------------------------------------------------------#
# This makefile was generated by 'cbp2make' tool rev.147                       #
#------------------------------------------------------------------------------#


WORKDIR = `pwd`

CC = gcc
CXX = g++
AR = ar
LD = g++
WINDRES = windres

INC = -Iinclude/FastNoiseSIMD
CFLAGS = -Wall -fexceptions --std=c++11 `Magick++-config --cppflags` -msse2 -msse4.1 -march=core-avx2
RESINC = 
LIBDIR = 
LIB = 
LDFLAGS = `Magick++-config --libs --ldflags`

INC_DEBUG = $(INC)
CFLAGS_DEBUG = $(CFLAGS) -g
RESINC_DEBUG = $(RESINC)
RCFLAGS_DEBUG = $(RCFLAGS)
LIBDIR_DEBUG = $(LIBDIR)
LIB_DEBUG = $(LIB)
LDFLAGS_DEBUG = $(LDFLAGS)
OBJDIR_DEBUG = obj/Debug
OBJDIR_LIB_DEBUG = obj/Debug/lib
DEP_DEBUG = 
OUT_DEBUG = bin/Debug/gaia
OBJ_DEBUG = $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD.o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_avx2.o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_avx512.o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_internal.o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_neon.o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_sse2.o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_sse41.o $(OBJDIR_DEBUG)/main.o

INC_RELEASE = $(INC)
CFLAGS_RELEASE = $(CFLAGS) -O2
RESINC_RELEASE = $(RESINC)
RCFLAGS_RELEASE = $(RCFLAGS)
LIBDIR_RELEASE = $(LIBDIR)
LIB_RELEASE = $(LIB)
LDFLAGS_RELEASE = $(LDFLAGS) -s
OBJDIR_RELEASE = obj/Release
OBJDIR_LIB_RELEASE = obj/Release/lib
DEP_RELEASE = 
OUT_RELEASE = bin/Release/gaia
OBJ_RELEASE = $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD.o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_avx2.o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_avx512.o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_internal.o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_neon.o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_sse2.o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_sse41.o $(OBJDIR_RELEASE)/main.o

all: debug release

clean: clean_debug clean_release

before_debug: 
	test -d bin/Debug || mkdir -p bin/Debug
	test -d $(OBJDIR_DEBUG) || mkdir -p $(OBJDIR_DEBUG)
	test -d $(OBJDIR_DEBUG)/lib/FastNoiseSIMD || mkdir -p $(OBJDIR_DEBUG)/lib/FastNoiseSIMD

after_debug: 

debug: before_debug out_debug after_debug

out_debug: before_debug $(OBJ_DEBUG) $(DEP_DEBUG)
	$(LD) $(LIBDIR_DEBUG) -o $(OUT_DEBUG) $(OBJ_DEBUG)  $(LDFLAGS_DEBUG) $(LIB_DEBUG)

$(OBJDIR_DEBUG)/main.o: main.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c main.cpp -o $(OBJDIR_DEBUG)/main.o
	
$(OBJDIR_DEBUG)/lib/FastNoiseSIMD/FastNoiseSIMD.o: lib/FastNoiseSIMD/FastNoiseSIMD.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c lib/FastNoiseSIMD/FastNoiseSIMD.cpp -o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD.o
	
$(OBJDIR_DEBUG)/lib/FastNoiseSIMD/FastNoiseSIMD_avx2.o: lib/FastNoiseSIMD/FastNoiseSIMD_avx2.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c lib/FastNoiseSIMD/FastNoiseSIMD_avx2.cpp -o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_avx2.o
	
$(OBJDIR_DEBUG)/lib/FastNoiseSIMD/FastNoiseSIMD_avx512.o: lib/FastNoiseSIMD/FastNoiseSIMD_avx512.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c lib/FastNoiseSIMD/FastNoiseSIMD_avx512.cpp -o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_avx512.o
	
$(OBJDIR_DEBUG)/lib/FastNoiseSIMD/FastNoiseSIMD_internal.o: lib/FastNoiseSIMD/FastNoiseSIMD_internal.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c lib/FastNoiseSIMD/FastNoiseSIMD_internal.cpp -o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_internal.o
	
$(OBJDIR_DEBUG)/lib/FastNoiseSIMD/FastNoiseSIMD_neon.o: lib/FastNoiseSIMD/FastNoiseSIMD_neon.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c lib/FastNoiseSIMD/FastNoiseSIMD_neon.cpp -o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_neon.o
	
$(OBJDIR_DEBUG)/lib/FastNoiseSIMD/FastNoiseSIMD_sse2.o: lib/FastNoiseSIMD/FastNoiseSIMD_sse2.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c lib/FastNoiseSIMD/FastNoiseSIMD_sse2.cpp -o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_sse2.o
	
$(OBJDIR_DEBUG)/lib/FastNoiseSIMD/FastNoiseSIMD_sse41.o: lib/FastNoiseSIMD/FastNoiseSIMD_sse41.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c lib/FastNoiseSIMD/FastNoiseSIMD_sse41.cpp -o $(OBJDIR_LIB_DEBUG)/FastNoiseSIMD/FastNoiseSIMD_sse41.o

clean_debug: 
	rm -f $(OBJ_DEBUG) $(OUT_DEBUG)
	rm -rf bin/Debug
	rm -rf $(OBJDIR_DEBUG)

before_release: 
	test -d bin/Release || mkdir -p bin/Release
	test -d $(OBJDIR_RELEASE) || mkdir -p $(OBJDIR_RELEASE)
	test -d $(OBJDIR_RELEASE)/lib/FastNoiseSIMD || mkdir -p $(OBJDIR_RELEASE)/lib/FastNoiseSIMD

after_release: 

release: before_release out_release after_release

out_release: before_release $(OBJ_RELEASE) $(DEP_RELEASE)
	$(LD) $(LIBDIR_RELEASE) -o $(OUT_RELEASE) $(OBJ_RELEASE)  $(LDFLAGS_RELEASE) $(LIB_RELEASE)

$(OBJDIR_RELEASE)/main.o: main.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c main.cpp -o $(OBJDIR_RELEASE)/main.o
	
$(OBJDIR_RELEASE)/lib/FastNoiseSIMD/FastNoiseSIMD.o: lib/FastNoiseSIMD/FastNoiseSIMD.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c lib/FastNoiseSIMD/FastNoiseSIMD.cpp -o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD.o
	
$(OBJDIR_RELEASE)/lib/FastNoiseSIMD/FastNoiseSIMD_avx2.o: lib/FastNoiseSIMD/FastNoiseSIMD_avx2.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c lib/FastNoiseSIMD/FastNoiseSIMD_avx2.cpp -o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_avx2.o
	
$(OBJDIR_RELEASE)/lib/FastNoiseSIMD/FastNoiseSIMD_avx512.o: lib/FastNoiseSIMD/FastNoiseSIMD_avx512.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c lib/FastNoiseSIMD/FastNoiseSIMD_avx512.cpp -o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_avx512.o
	
$(OBJDIR_RELEASE)/lib/FastNoiseSIMD/FastNoiseSIMD_internal.o: lib/FastNoiseSIMD/FastNoiseSIMD_internal.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c lib/FastNoiseSIMD/FastNoiseSIMD_internal.cpp -o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_internal.o
	
$(OBJDIR_RELEASE)/lib/FastNoiseSIMD/FastNoiseSIMD_neon.o: lib/FastNoiseSIMD/FastNoiseSIMD_neon.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c lib/FastNoiseSIMD/FastNoiseSIMD_neon.cpp -o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_neon.o
	
$(OBJDIR_RELEASE)/lib/FastNoiseSIMD/FastNoiseSIMD_sse2.o: lib/FastNoiseSIMD/FastNoiseSIMD_sse2.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c lib/FastNoiseSIMD/FastNoiseSIMD_sse2.cpp -o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_sse2.o
	
$(OBJDIR_RELEASE)/lib/FastNoiseSIMD/FastNoiseSIMD_sse41.o: lib/FastNoiseSIMD/FastNoiseSIMD_sse41.cpp
	$(CXX) $(CFLAGS_RELEASE) $(INC_RELEASE) -c lib/FastNoiseSIMD/FastNoiseSIMD_sse41.cpp -o $(OBJDIR_LIB_RELEASE)/FastNoiseSIMD/FastNoiseSIMD_sse41.o
	
clean_release: 
	rm -f $(OBJ_RELEASE) $(OUT_RELEASE)
	rm -rf bin/Release
	rm -rf $(OBJDIR_RELEASE)

prefix=/usr/local

install:
	install -m 0755 bin/Release/gaia $(prefix)/bin

.PHONY: before_debug after_debug clean_debug before_release after_release clean_release install

