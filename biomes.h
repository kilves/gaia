#pragma once

#include "Magick++.h"

// Where the ocean should be.
const float OCEAN_LEVEL = 0.6f;
// How steep the transitions between three humidity zones should be
const float RAINFALL_ZONE_STEEPNESS = 4.0f;
// Being inland makes temperatures more extreme. This value determines how extreme
const float INLAND_TEMPERATURE_EXTREMENESS = 2.0f;
// Same thing with rainfalls.
const float INLAND_RAINFALL_EXTREMENESS = 2.0f;
// How cold it gets in higher altitudes
const float ELEVATION_COLDNESS = 1.0f;

// Color definitions for different biomes.
const MagickLib::FloatPixelPacket DESERT =              { 0.75f, 0.75f, 0.00f, 1.0f };
const MagickLib::FloatPixelPacket TUNDRA =              { 0.7f, 0.7f, 0.7f, 1.0f };
const MagickLib::FloatPixelPacket RAINFOREST =          { 0.0f, 0.2f, 0.0f, 1.0f };
const MagickLib::FloatPixelPacket SEASONALFOREST =      { 0.0f, 0.3f, 0.0f, 1.0f };
const MagickLib::FloatPixelPacket SAVANNA =             { 0.25f, 0.3f, 0.0f, 1.0f };
const MagickLib::FloatPixelPacket WOODS =               { 0.0f, 0.4f, 0.0f, 1.0f };
const MagickLib::FloatPixelPacket FOREST =              { 0.0f, 0.35f, 0.0f, 1.0f };
const MagickLib::FloatPixelPacket SWAMP =               { 0.15f, 0.35f, 0.05f, 1.0f };
const MagickLib::FloatPixelPacket TAIGA =               { 0.3f, 0.5f, 0.3f, 1.0f };
const MagickLib::FloatPixelPacket GRASSLAND =           { 0.5f, 0.5f, 0.3f, 1.0f };
const MagickLib::FloatPixelPacket WATER =               { 0.0f, 0.0f, 1.0f, 1.0f };
const MagickLib::FloatPixelPacket WHITE =               { 1.0f, 1.0f, 1.0f, 1.0f };
const MagickLib::FloatPixelPacket BLACK =               { 0.0f, 0.0f, 0.0f, 1.0f };

// Biome zones themselves. A 10x10 grid
// Horizontal: humidity, dry -> wet
// Vertical: temperature, cold -> hot
const MagickLib::FloatPixelPacket BIOME[100] = {
    TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA,
    TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA, TUNDRA,
    GRASSLAND, GRASSLAND, TAIGA, TAIGA, TAIGA, TAIGA, TAIGA, TAIGA, TAIGA, TAIGA,
    GRASSLAND, GRASSLAND, TAIGA, TAIGA, TAIGA, TAIGA, TAIGA, TAIGA, TAIGA, TAIGA,
    GRASSLAND, GRASSLAND, WOODS, WOODS, FOREST, FOREST, FOREST, FOREST, SWAMP, SWAMP,
    DESERT, DESERT, WOODS, WOODS, FOREST, FOREST, FOREST, FOREST, SWAMP, SWAMP,
    DESERT, DESERT, SAVANNA, SAVANNA, SEASONALFOREST, SEASONALFOREST, SEASONALFOREST, SEASONALFOREST, SWAMP, SWAMP,
    DESERT, DESERT, SAVANNA, SAVANNA, SEASONALFOREST, SEASONALFOREST, SEASONALFOREST, SEASONALFOREST, SWAMP, SWAMP,
    DESERT, DESERT, SAVANNA, SAVANNA, RAINFOREST, RAINFOREST, RAINFOREST, RAINFOREST, RAINFOREST, RAINFOREST,
    DESERT, DESERT, SAVANNA, SAVANNA, RAINFOREST, RAINFOREST, RAINFOREST, RAINFOREST, RAINFOREST, RAINFOREST
};
