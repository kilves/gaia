# Gaia

Gaia is a command-line utility written in C++ that generates random planet textures based on a simple weather model.

## Installation

```
make
sudo make install
```

There's no configure script yet.

## Usage

```
gaia [-h] [-o outfile] [-h height] [-s seed] [-O oceanLevel] [-rs rainSteep] [-re rainExtreme] [-te tempExtreme] [-ec elevColdness]

Options:
  -h               print this message and exit.
  -o outfile       the image file to save the generated world to. Default: terrain.png
  -h height        the height of the saved image. Default: 400. Width will always be height*2.
  -s seed          the seed used to generate the world. Default: 1337
  -O oceanLevel    all altitudes below this will be filled with ocean. (0.0 ... 1.0). Default: 0.6
  -rs rainSteep    how steep the transitions between the different rain zones should be. Can be any decimal, even negative. Default: 4.0
  -re rainExtreme  how extreme the rainfall should get as you get more inland. Dry becomes dryer, wet becomes more wet etc. Default: 2.0
  -te tempExtreme  same as rainExtreme, but with temperature. Default: 2.0
  -ec elevColdness how cold it should get as you get higher up. Default: 1.0
```

## Example

just running `gaia` produces a texture like this:
![Example image 1](https://i.imgur.com/UVJ9uoN.png)

## Development dependencies

* [Magick++](https://www.imagemagick.org/Magick++/) for generating the images.

## Credits

* Ben Hallett [wrote](https://4026.me.uk/#projects_worldgen) a nice blogpost that inspired me with the implementation of the biome system.
* I got the idea for some of my implementations from [this](https://www.redblobgames.com/maps/terrain-from-noise/) post.
