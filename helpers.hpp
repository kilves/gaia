#pragma once
#include <cmath>
#include <iostream>
#include "Magick++.h"

const float PI = 3.14159;
const float PI2 = PI * 2;
using namespace std;

struct GeneratorParameters {
    float oceanLevel, rainfallSteepness, temperatureExtremeness, rainfallExtremeness, elevationColdness;
    int height, seed;
    string outfile;
};

void printUsage() {
    cout << "Usage:" << endl <<
            "  gaia [-h] [-o outfile] [-h height] [-s seed] [-O oceanLevel] [-rs rainSteep] [-re rainExtreme] [-te tempExtreme] [-ec elevColdness] " << endl <<
            endl <<
            "Options:" << endl <<
            "  -h               print this message and exit." << endl <<
            "  -o outfile       the image file to save the generated world to. Default: terrain.png" << endl <<
            "  -h height        the height of the saved image. Default: 400. Width will always be height*2" << endl <<
            "  -s seed          the seed used to generate the world. Default: 1337" << endl <<
            "  -O oceanLevel    all altitudes below this will be filled with ocean. (0.0 ... 1.0). Default: 0.6" << endl <<
            "  -rs rainSteep    how steep the transitions between the different rain zones should be. Can be any decimal, even negative. Default: 4.0" << endl <<
            "  -re rainExtreme  how extreme the rainfall should get as you get more inland. Dry becomes dryer, wet becomes more wet etc. Default: 2.0" << endl <<
            "  -te tempExtreme  same as rainExtreme, but with temperature. Default: 2.0" << endl <<
            "  -ec elevColdness how cold it should get as you get higher up. Default: 1.0" << endl;
}

void parseParameters(GeneratorParameters& parameters, int argc, char** argv) {
    for(int i = 1; i < argc; i++) {
        if(strcmp(argv[i], "-h") == 0) {
            printUsage();
            exit(EXIT_SUCCESS);
        }
        if(strcmp(argv[i], "-o") == 0 && i < argc - 1) {
            parameters.outfile = string(argv[i+1]);
            i++; continue;
        }
        if(strcmp(argv[i], "-h") == 0 && i < argc - 1) {
            try {
                parameters.height = stoi(string(argv[i+1]));
            } catch (const exception& e) {
                cout << "Image height must be a valid integer" << endl;
                exit(EXIT_SUCCESS);
            }
            i++; continue;
        }
        if(strcmp(argv[i], "-O") == 0 && i < argc - 1) {
            try {
                parameters.oceanLevel = stof(string(argv[i+1]));
            } catch (const exception& e) {
                cout << "Ocean level must be a valid decimal." << endl;
                exit(EXIT_SUCCESS);
            }
            i++; continue;
        }
        if(strcmp(argv[i], "-rs") == 0 && i < argc - 1) {
            try {
                parameters.rainfallSteepness = stof(string(argv[i+1]));
            } catch (const exception& e) {
                cout << "Rainfall steepness must be a valid decimal" << endl;
                exit(EXIT_SUCCESS);
            }
            i++; continue;
        }
        if(strcmp(argv[i], "-re") == 0 && i < argc - 1) {
            try {
                parameters.rainfallExtremeness = stof(string(argv[i+1]));
            } catch (const exception& e) {
                cout << "Rainfall extremeness must be a valid decimal" << endl;
                exit(EXIT_SUCCESS);
            }
            i++; continue;
        }
        if(strcmp(argv[i], "-te") == 0 && i < argc - 1) {
            try {
                parameters.temperatureExtremeness = stof(string(argv[i+1]));
            } catch (const exception& e) {
                cout << "Temperature extremeness must be a valid decimal" << endl;
                exit(EXIT_SUCCESS);
            }
            i++; continue;
        }
        if(strcmp(argv[i], "-ec") == 0 && i < argc - 1) {
            try {
                parameters.elevationColdness = stof(string(argv[i+1]));
            } catch (const exception& e) {
                cout << "Elevation coldness must be a valid decimal" << endl;
                exit(EXIT_SUCCESS);
            }
            i++; continue;
        }
        if(strcmp(argv[i], "-s") == 0 && i < argc - 1) {
            try {
                parameters.seed = stoi(string(argv[i+1]));
            } catch (const exception& e) {
                cout << "Seed must be a valid integer." << endl;
                exit(EXIT_SUCCESS);
            }
            i++; continue;
        }
        printUsage();
        exit(EXIT_SUCCESS);
    }
}

float lerp(float a, float b, float x) {
    return a + (b - a) * x;
}

void polarToCartesian(float lat, float lon, float r, float& x, float& y, float& z) {
    x = r * sin(lon) * cos(lat);
    y = r * sin(lon) * sin(lat);
    z = r * cos(lon);
}

void cartesianToPolar(float x, float y, float z, float& lat, float& lon, float& r) {
    r = sqrt(x*x + y*y + z*z);
    lat = acos(z/r);
    lon = atan(y/z);
}

MagickLib::FloatPixelPacket lerp(MagickLib::FloatPixelPacket a, MagickLib::FloatPixelPacket b, float x) {
    return {a.red + (b.red - a.red) * x,
            a.green + (b.green - a.green) * x,
            a.blue + (b.blue - a.blue) * x,
            1.0f};
}

MagickLib::FloatPixelPacket bicubify(MagickLib::FloatPixelPacket a, MagickLib::FloatPixelPacket b, MagickLib::FloatPixelPacket c, MagickLib::FloatPixelPacket d, float x, float y) {
    return {lerp(lerp(a.red, b.red, x), lerp(c.red, d.red, x), y),
            lerp(lerp(a.green, b.green, x), lerp(c.green, d.green, x), y),
            lerp(lerp(a.blue, b.blue, x), lerp(c.blue, d.blue, x), y),
            1.0f};
}

void GetHeightPixel(MagickLib::FloatPixelPacket& pixel, float elevation) {
    elevation = (elevation + 1.0f) / 2.0f;
    pixel = {elevation, elevation, elevation, 1.0f};
}
